package com.xf.oschina.api;

import android.arch.lifecycle.LiveData;

import com.xf.oschina.module.book.domain.BookData;
import com.xf.oschina.module.book.domain.CommentData;
import com.xf.oschina.utils.ApiResponse;

import java.util.Map;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface ObjectApiService {

    /**
     * @author xf
     * @time 2018/7/5  15:33
     * @describe 获取图书列表
     */
    @GET("v2/book/search")
    LiveData<ApiResponse<BookData>> getBooks(@QueryMap Map<String, String> params);

    //@Path("bookId") String bookId, @Query("start") int start, @Query("count") int count, @Query("fields") String fields
    @GET("v2/book/{bookId}/reviews")
    LiveData<ApiResponse<CommentData>> getBookReviews(@Path("bookId") String bookId, @QueryMap Map<String, String> params);

//    @GET("book/series/{seriesId}/books")
//    Observable<Response<BookSeriesListResponse>> getBookSeries(@Path("seriesId") String seriesId, @Query("start") int start, @Query("count") int count, @Query("fields") String fields);
}
