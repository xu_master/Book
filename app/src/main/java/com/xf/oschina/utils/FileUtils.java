package com.xf.oschina.utils;

import com.xf.oschina.module.story.domain.ChapterRead;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileUtils {
    public static void saveChapter(ChapterRead.Chapter chapter) {
        try {
            String filePath = "sdcard/book/" + chapter.getChapterId() + ".txt";
            File file = new File(filePath);
            if (!file.exists()) {
                FileOutputStream outputStream = new FileOutputStream(file);
                outputStream.write(chapter.getBody().getBytes());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
