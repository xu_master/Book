package com.xf.oschina.view.book.listener;

public interface PageTurningListener {
    void onNext(String text);

    void onForword(String text);
}
