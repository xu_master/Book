package com.xf.oschina.view.book.factory;

import com.xf.oschina.module.story.domain.Pager;

import java.util.ArrayList;
import java.util.List;

public abstract class LoadPagerFactory {
    //行间距
    protected int mTextInterval;
    //行间距
    protected int mInterval;
    //标题的行间距
    protected int mTitleInterval;
    protected int screenHeight, screenWidth;
    protected int contentHeight;
    protected float mVisibleWidth, mVisibleHeight;
    protected int wordCount;
    protected int lineSpn = 0;//这里是换行符计算
    protected int tempPagerCount = 0;
    protected List<Pager> pagers = new ArrayList<>();
    protected List<Pager> pagersTemp = new ArrayList<>();
    protected boolean isNext = true;
    protected float leftMargin, rightMargin, topMargin, bottomMargin;
    public int getmTextInterval() {
        return mTextInterval;
    }

    public int getInterval() {
        return mInterval;
    }

    public int getTitleInterval() {
        return mTitleInterval;
    }

    public int getScreenHeight() {
        return screenHeight;
    }

    public int getScreenWidth() {
        return screenWidth;
    }

    public int getContentHeight() {
        return contentHeight;
    }

    public float getmVisibleWidth() {
        return mVisibleWidth;
    }

    public float getVisibleHeight() {
        return mVisibleHeight;
    }

    public int getWordCount() {
        return wordCount;
    }

    public int getLineSpn() {
        return lineSpn;
    }

    public int getTempPagerCount() {
        return tempPagerCount;
    }

    public float getLeftMargin() {
        return leftMargin;
    }

    public float getRightMargin() {
        return rightMargin;
    }

    public float getTopMargin() {
        return topMargin;
    }

    public float getBottomMargin() {
        return bottomMargin;
    }
    abstract String loadNextPager(int next);

    abstract String loadForWardPager(int forward);
}
