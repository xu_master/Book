package com.xf.oschina.module.movie.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;

import com.xf.oschina.R;
import com.xf.oschina.base.BaseFragment;
import com.xf.oschina.databinding.FragmentMovieBinding;

public class MovieFragment extends BaseFragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentMovieBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_movie, container, false);
        binding.web.getSettings().setJavaScriptEnabled(true);
//        binding.web.loadUrl("http://61.240.234.226/CaseDaughter/CaseDaughter?mobile=18627556875");
//        binding.web.setWebViewClient(new WebViewClient() {
//            //覆盖shouldOverrideUrlLoading 方法
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                view.loadUrl(url);
//                return true;
//            }
//
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                super.onPageFinished(view, url);
//                view.loadUrl("javascript:(function(){ " +
//                        "                  var iFrame;" +
//                        "                 iFrame = document.createElement('iframe');" +
//                        "  iFrame.setAttribute('height', '1000px');" +
//                        "    iFrame.setAttribute('width', '720px');" +
//                        "                 iFrame.setAttribute('src', 'http://61.240.234.226/CaseDaughter/CaseDaughter?mobile=18627556875');" +
//                        "                 document.body.appendChild(iFrame);" +
//                        "               };)()");
//            }
//        });
//        binding.web.setWebChromeClient(new WebChromeClient() {
//        });
//        binding.web.addJavascriptInterface(new JSBridge(), "android");
        // 这段js函数的功能就是，遍历所有的img几点，并添加onclick函数，在还是执行的时候调用本地接口传递url过去

        return binding.getRoot();
    }

    @Override
    public void retry() {

    }

    @Override
    public void refresh() {

    }


    public class JSBridge {
        @JavascriptInterface
        public void addIframe() {
        }
    }
}
