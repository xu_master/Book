package com.xf.oschina.module.story.domain;

/**
 * @author xf
 * @time 2018/8/10  16:18
 * @describe 分页bean类
 */
public class Pager {
    private String content;
    private int pagerIndex;
    private int chapterId;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getPagerIndex() {
        return pagerIndex;
    }

    public void setPagerIndex(int pagerIndex) {
        this.pagerIndex = pagerIndex;
    }

    public int getChapterId() {
        return chapterId;
    }

    public void setChapterId(int chapterId) {
        this.chapterId = chapterId;
    }
}
