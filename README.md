# Book

#### 项目介绍
基于MVVM架构的开源demo，不做商用仅做学习用，接口是用的豆瓣和ebook的，如有侵权立删
项目截图如下：
![输入图片说明](https://images.gitee.com/uploads/images/2018/0718/093813_e300869e_789921.png "QQ图片20180718093513.png")
                           ![输入图片说明](https://images.gitee.com/uploads/images/2018/0718/093825_91912108_789921.png "QQ图片20180718093620.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/0718/093834_8c06b256_789921.png "QQ图片20180718093641.png")
                           ![输入图片说明](https://images.gitee.com/uploads/images/2018/0718/093843_6f5c4b16_789921.png "QQ图片20180718093659.png")
#### 软件架构
软件架构基于MVVM开发，
主要框架：
- dagger2注解框架：[dagger2](https://github.com/google/dagger)
- greendao数据库框架：[greendao](https://github.com/greenrobot/greenDAO)
- glide图片加载框架：[glide](https://github.com/bumptech/glide)
- SmartRefreshLayout刷新控件：[SmartRefreshLayout](https://github.com/scwang90/SmartRefreshLayout)
- swipebacklayout侧滑退出activity：[swipebacklayout](https://github.com/scwang90/SmartRefreshLayout)
- 内存泄漏检测工具：[leakcanary](https://github.com/square/leakcanary)
- 网络请求框架：[retrofit](https://github.com/square/leakcanary)
- google官方生命周期管理框架：[architecture](https://github.com/googlesamples/android-architecture)


#### 安装教程

直接克隆下来就可以使用


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)